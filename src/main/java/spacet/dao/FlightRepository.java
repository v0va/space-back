package spacet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import spacet.entities.Flight;

public interface FlightRepository extends JpaRepository<Flight, Long> {
}
